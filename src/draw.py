from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QFont, QPainter, QPen

from logic import Labyrinth 


class QT5LabyrinthDraw: 
    
    COLOR_LIGHT_GRAY_2 = QColor(233, 233, 233)
    COLOR_LIGHT_GRAY = QColor(192, 192, 192)
    COLOR_GRAY = QColor(128, 128, 128)
    COLOR_WHITE = Qt.white
    COLOR_PATH = Qt.red
    
    def __init__(self, board: Labyrinth):
        self.board = board
        self.modifiers = 0
    
    def draw_styled_3d_rectangle(self, qpainter: QPainter, x: int, y: int, width: int, height: int,
                                 length: int=2, inverted: bool=False):
        color1, color2 = self.COLOR_WHITE, self.COLOR_GRAY
        if inverted:
            color1, color2 = color2, color1
        
        qpainter.fillRect(x, y, width, height, self.COLOR_LIGHT_GRAY)
        qpainter.fillRect(x, y, width, length, color1)
        qpainter.fillRect(x, y, length, height, color1)
        qpainter.fillRect(x + 1, y + 1 + height - length, width, length, color2)
        qpainter.fillRect(x + 1 + width - length, y + 1, length, height, color2)
        
    def draw_board_grid(self, qpainter: QPainter):
        board = self.board
        qpainter.setPen(self.COLOR_GRAY)
        off_left, off_top = board.px_off_left, board.px_off_top
        
        line_x1 = board.px_off_left + board.width * board.cell
        for line in range(board.height + 1):
            line_y = off_top + line * board.cell
            qpainter.drawLine(off_left, line_y, line_x1, line_y)
        
        line_y1 = board.px_off_top + board.height * board.cell
        for column in range(board.width + 1):
            line_x = off_left + column * board.cell
            qpainter.drawLine(line_x, off_top, line_x, line_y1)
    
    def draw_wall_cell(self, qpainter: QPainter, x: int, y: int):
        board = self.board
        cell_1 = board.cell - 1
        qpainter.fillRect(x, y, board.cell, board.cell, self.COLOR_GRAY)
        qpainter.fillRect(x + 1, y + 1, cell_1, cell_1, self.COLOR_LIGHT_GRAY)
        
    def draw_clear_cell(self, qpainter: QPainter, x: int, y: int):
        board = self.board
        qpainter.fillRect(x + 1, y + 1, board.cell, board.cell, self.COLOR_LIGHT_GRAY_2)
    
    def draw_cell_text(self, qpainter: QPainter, px_x: int, px_y: int, text):
        text_rect = (px_x + 1, px_y, self.board.cell, self.board.cell)
        font = QFont("Arial", self.board.cell * 0.47)
        font.setWeight(QFont.Black)
        if isinstance(text, int) and text >= 100:
            font.setPointSize(self.board.cell * 0.41)
        qpainter.setFont(font)
        qpainter.drawText(*text_rect, Qt.AlignCenter | Qt.AlignHCenter, str(text))
    
    def draw_cells(self, qpainter: QPainter):
        board = self.board
        displayNumbers = self.modifiers & Qt.ShiftModifier
        for y in range(board.height):
            for x in range(board.width):
                value = board.get_cell_value(x, y)
                
                px_x, px_y = board.get_cell_pos(x, y)

                if value == -1: 
                    # self.draw_wall_cell(qpainter, px_x, px_y)            
                    self.draw_styled_3d_rectangle(qpainter, px_x, px_y, board.cell - 1, board.cell - 1)
                elif value == -2:
                    self.draw_clear_cell(qpainter, px_x, px_y)
                    self.draw_cell_text(qpainter, px_x, px_y, "Н")
                elif value == -2:
                    self.draw_clear_cell(qpainter, px_x, px_y)
                    self.draw_cell_text(qpainter, px_x, px_y, "К")
                else:
                    self.draw_clear_cell(qpainter, px_x, px_y)
                    if value != 0 and displayNumbers:
                        self.draw_cell_text(qpainter, px_x, px_y, value)                        
    
    def draw_path(self, qpainter: QPainter):
        path = self.board.path
        cell_2 = self.board.cell / 2
        if path:
            qpen = QPen(self.COLOR_PATH)
            qpen.setWidth(2)
            qpainter.setPen(qpen)
            
            for i in range(len(path) - 1):
                x0, y0 = self.board.get_cell_pos(*path[i])
                x1, y1 = self.board.get_cell_pos(*path[i + 1])
                
                qpainter.drawLine(x0 + cell_2, y0 + cell_2, x1 + cell_2, y1 + cell_2)
    
    def draw(self, qpainter: QPainter): 
        board = self.board
        
        # рамка окна
        px_x, px_y = board.px_off_left, board.px_off_top
        px_x1, px_y1 = board.px_off_right, board.px_off_bottom
        px_w, px_h = board.cell * board.width, board.cell * board.height
        self.draw_styled_3d_rectangle(qpainter, 0, 0, px_x + px_w + px_x1, px_y + px_h + px_y1, 3)
        
        # рамка поля 
        d = 3
        d2 = d * 2
        self.draw_styled_3d_rectangle(qpainter, px_x - d, px_y - d, px_w + d2, px_h + d2, 3, True)
        
        # сетка, поле
        self.draw_board_grid(qpainter)
        self.draw_cells(qpainter)
        
        # путь
        self.draw_path(qpainter) 
