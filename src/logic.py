from collections import deque
import random


class Labyrinth:
    
    def __init__(self, width: int, height: int, cell_size: int):
        self.width = width
        self.height = height
        self.cell = cell_size
        self.cells = [[-1 for _ in range(width)] for _ in range(height)]
        self.path = []
        
        self.set_display_offset(0, 0, 0, 0)
    
    def set_display_offset(self, left: int, right: int, top: int, bottom: int):
        self.px_off_left = left
        self.px_off_right = right
        self.px_off_top = top
        self.px_off_bottom = bottom
        
    def get_cell_pos(self, x: int, y: int) -> (int, int):
        px_x = self.px_off_left + x * self.cell
        px_y = self.px_off_top + y * self.cell
        return px_x, px_y
    
    def is_pos_valid(self, x: int, y: int) -> bool:
        return 0 <= x < self.width and 0 <= y < self.height
    
    def get_cell(self, px_x: int, px_y: int) -> (int, int):
        x = int((px_x - self.px_off_left) // self.cell)
        y = int((px_y - self.px_off_top) // self.cell)
        
        if self.is_pos_valid(x, y):
            return x, y
        return -1, -1
    
    def get_cell_value(self, x: int, y: int) -> int:
        return self.cells[y][x]
    
    def set_cell_value(self, x: int, y: int, value: int):
        self.cells[y][x] = value
    
    def leee(self, x0, y0, x1, y1) -> int: 
        q = deque()
        q.append((x0, y0, 1))
     
        min_dist = -1
        while q:
            x, y, dist = q.popleft()
            self.cells[y][x] = dist
            if x == x1 and y == y1:
                min_dist = dist
                break
     
            for dx, dy in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
                xn = x + dx
                yn = y + dy
                
                if 0 <= xn < self.width and 0 <= yn < self.height and self.cells[yn][xn] == 0:
                    element = (xn, yn, dist + 1)
                    if element not in q:
                        q.append(element)
        
        if min_dist == -1:
            return
        
        points = [(x1, y1)]
        
        x, y, d = x1, y1, min_dist
        while d != 1:
            for dx, dy in [(0, -1), (-1, 0), (0, 1), (1, 0)]:
                xn = x + dx
                yn = y + dy
                if 0 <= xn < self.width and 0 <= yn < self.height and 0 < self.cells[yn][xn] < d:
                    points.append((xn, yn))
                    x, y = xn, yn
                    d = self.cells[yn][xn]
                    break
        
        self.path = points
    
    def generate(self):
        width_range = range(0, self.width)
        height_range = range(0, self.height)
        
        for x in random.sample(width_range, k=self.width):
            for y in random.sample(height_range, k=self.height):
                rand = random.random()
                if rand > 0.35:
                    self.cells[y][x] = 0
        
        self.cells[0][0] = 0
        self.cells[-1][-1] = 0
        
