import sys
import os

from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtGui import QIcon, QPainter
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction
from PyQt5.QtWidgets import QFileDialog, QDialog, QMessageBox, QFormLayout, QDialogButtonBox, QSpinBox

from draw import QT5LabyrinthDraw
from logic import Labyrinth


class MWindow(QMainWindow):
    
    def __init__(self):
        super().__init__()
        
        self.board = None
        self.drawer = None
        self.file_path = None
        
        self.newPoints = None
        self.points = [(0, 0), (-1, -1)]
        
        self.mouseButton = -1
        
        self.initUI()
        self.initMenu()
    
    def initUI(self): 
        self.setFixedSize(200, 200)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowMaximizeButtonHint)
        self.setWindowTitle("Лабиринт")
        self.setWindowIcon(QIcon("jokerge.png"))
        
    def initMenu(self):
        menu = self.menuBar().addMenu("Меню")
        
        genMap = QAction("Сгенерировать карту", self)
        genMap.setShortcut("Ctrl+A")
        genMap.triggered.connect(self.genMap)
        menu.addAction(genMap)
        
        newMap = QAction("Создать карту", self)
        newMap.setShortcut("Ctrl+N")
        newMap.triggered.connect(self.newMap)
        menu.addAction(newMap)
        
        openMap = QAction("Открыть карту", self)
        openMap.setShortcut("Ctrl+O")
        openMap.triggered.connect(self.openMap)
        menu.addAction(openMap)
        
        reload = QAction("Перезагрузить карту", self)
        reload.setShortcut("Ctrl+R")
        reload.triggered.connect(self.reload)
        menu.addAction(reload)
        
        menu.addSeparator()
         
        saveMap = QAction("Сохранить карту", self)
        saveMap.setShortcut("Ctrl+S")
        saveMap.triggered.connect(self.saveMap)
        menu.addAction(saveMap)
        
        saveResults = QAction("Сохранить результат", self)
        saveResults.setShortcut("Ctrl+E")
        saveResults.triggered.connect(self.saveResults)
        menu.addAction(saveResults)
        
        menu.addSeparator()
        
        self.editMode = QAction("Режим редактирования", self)
        self.editMode.setCheckable(True)
        self.editMode.triggered.connect(self.toggleEdit)
        menu.addAction(self.editMode)
        
        changePoints = QAction("Изменить крайние точки", self)
        changePoints.triggered.connect(self.changePoints)
        menu.addAction(changePoints)
        
        menu.addSeparator()
        
        exit_ = QAction("Выход", self)
        exit_.setShortcut("Ctrl+Q")
        exit_.triggered.connect(sys.exit)
        menu.addAction(exit_)
        
        helpMenu = self.menuBar().addMenu("Помощь")
        about = QAction("О программе", self)
        about.setShortcut("F1")
        about.triggered.connect(self.showCredits)
        helpMenu.addAction(about)
    
    def showCredits(self):
        window = QMessageBox(self)
        window.setWindowTitle(f"О программе")
        window.setText("Визуализация лабиринта для курсовой работы на тему\n«Реализация алгоритма поиска путей в лабиринте»\n\nАмиров Имран, 2023")
        window.show()
    
    def newLabyrinth(self, width: int, height: int, cells: [[int]]=None):
        board = Labyrinth(width, height, 16)
        board.set_display_offset(10, 10, 28, 8)
        if cells:
            board.cells = cells
        self.board = board
        self.points = [(0, 0), (-1, -1)]
        
        px_width = width * board.cell + board.px_off_left + board.px_off_right
        px_height = height * board.cell + board.px_off_top + board.px_off_bottom
        self.setFixedSize(px_width, px_height)
        self.drawer = QT5LabyrinthDraw(self.board)
    
    def changePoints(self):
        if not self.board:
            return
        if self.editMode.isChecked():
            QMessageBox.information(self, self.windowTitle(), f"Сначала отключите режим редактирования")
            return
        
        if not getattr(self, "infoShown", None):
            QMessageBox.information(self, self.windowTitle(), "Нажмите левой кнопкой мыши "
                                "на начальную точку, затем на конечную")
            self.infoShown = True
        self.newPoints = []
    
    def requestMapDimensions(self) -> (int, int):
        dialog = QDialog(self)
        dialog.setWindowTitle("Новая карта")
        layout = QFormLayout(dialog)
        
        dialog.inputs = []
        for name, attr in [("Ширина:", "width"), ("Высота:", "height")]:
            spinbox = QSpinBox(dialog)
            spinbox.setRange(5, 50)
            value = getattr(self.board, attr) if self.board else 10
            spinbox.setValue(value)
            dialog.inputs.append(spinbox)
            layout.addRow(name, spinbox)
            
        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, dialog)
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        layout.addWidget(buttonBox)
        
        if dialog.exec():
            width, height = [i.value() for i in dialog.inputs]
            return width, height
        return 0, 0
    
    def newMap(self):
        width, height = self.requestMapDimensions()
        if width and height:
            self.newLabyrinth(width, height)
            self.editMode.setChecked(True)
    
    def genMap(self):
        width, height = self.requestMapDimensions()
        if width and height:
            self.newLabyrinth(width, height)
            self.board.generate()
            self.editMode.setChecked(False)
            self.solve()
            
    def solve(self):
        if self.points[1] == (-1, -1):
            self.points[1] = self.board.width - 1, self.board.height - 1
            
        self.board.leee(*self.points[0], *self.points[1])
        if not self.board.path:
            QMessageBox.information(self, self.windowTitle(), f"Путь не найден")

    def reload(self):
        self.openMap(file_path=self.file_path)
        
    def openMap(self, _=False, file_path=None): 
        if file_path is None:
            file_path, _ = QFileDialog.getOpenFileName(None, "Выберите файл с картой лабиринта", "./", "Текстовые файлы (*.txt);;Все файлы (*)")
            if not file_path:
                return
        if not os.path.isfile(file_path):
            QMessageBox.warning(self, self.windowTitle(), f"Файл не найден")
            return
        self.file_path = file_path
        
        file_stats = os.stat(file_path)
        if file_stats.st_size > 16384:
            QMessageBox.warning(self, self.windowTitle(), f"Размер файла не может быть больше 16 КБ")
            return
            
        lines = []
        try:
            with open(file_path, encoding="utf-8") as file:
                lines = file.read().splitlines()
        except Exception as e:
            QMessageBox.critical(self, self.windowTitle(), f"Произошла ошибка при чтении файла:\n{e}")
        if not lines:
            QMessageBox.information(self, self.windowTitle(), "Файл пустой")
            return
        
        # множество всех символов 
        char_set = set()
        for line in lines:
            line_chars = set(line)
            char_set.update(line_chars)
        
        if len(char_set) != 2:  # должен остаться только символ, обозначающий стену
            if not QApplication.keyboardModifiers() & Qt.ShiftModifier:
                QMessageBox.warning(self, self.windowTitle(), f"Файл не является картой лабиринта")
                return
        
        height = len(lines)
        
        # обработка строк - поиск самой длинной для выравнивания длин            
        width = len(max(lines, key=lambda i: len(i)))
        for line_index in range(height):
            lines[line_index] = lines[line_index].ljust(width, " ")
        
        # преобразуем карту в матрицу стен и дорог
        for line_index in range(height):
            lines[line_index] = [0 if i == " " else -1 for i in lines[line_index]]
             
        self.newLabyrinth(width, height, lines)
        if not self.editMode.isChecked():
            self.solve()
        self.repaint()
    
    def saveResults(self):
        if not self.board or not self.board.cells:
            QMessageBox.information(self, self.windowTitle(), f"Нечего сохранять")
            return
        
        file_text = ""
        max_dst = max([max(i) for i in self.board.cells])
        max_dst_len_2 = len(str(max_dst)) // 2 + 1

        for m_line in self.board.cells:
            for value in m_line:
                if value == -1:
                    file_text += "#".rjust(max_dst_len_2).ljust(max_dst_len_2) + " "
                else:
                    file_text += str(value).rjust(max_dst_len_2).ljust(max_dst_len_2) + " "
                
            file_text += "\n"
        
        file_path, _ = QFileDialog.getSaveFileName(None, "Выберите файл для сохранения результатов", "./", "Текстовые файлы (*.txt);;Все файлы (*)")
        if not file_path:
            return
        
        try:
            with open(file_path, encoding="utf-8", mode="w") as file:
                file.write(file_text)
        except Exception as e:
            QMessageBox.critical(self, self.windowTitle(), f"Произошла ошибка при сохранении результатов:\n{e}")
    
    def saveMap(self):
        if not self.board or not self.board.cells:
            QMessageBox.information(self, self.windowTitle(), f"Нечего сохранять")
            return
        
        file_text = ""
        for m_line in self.board.cells:
            for value in m_line:
                if value == -1:
                    file_text += "#"
                else:
                    file_text += " "
            file_text += "\n"
        
        file_path, _ = QFileDialog.getSaveFileName(None, "Выберите файл для сохранения карты лабиринта", "./", "Текстовые файлы (*.txt);;Все файлы (*)")
        if not file_path:
            return
        
        try:
            with open(file_path, encoding="utf-8", mode="w") as file:
                file.write(file_text)
        except Exception as e:
            QMessageBox.critical(self, self.windowTitle(), f"Произошла ошибка при сохранении карты лабиринта:\n{e}")
    
    def resetBoard(self):
        self.board.path = []
        for line in self.board.cells:
            for i in range(self.board.width):
                if line[i] > 0:
                    line[i] = 0
                        
    def toggleEdit(self, state: bool):
        if not self.board:
            return
        
        if state:
            self.resetBoard()
        else:
            self.solve()
        self.repaint()
    
    def paintEvent(self, event: QEvent):
        qpainter = QPainter()
        qpainter.begin(self)
        if self.drawer:
            self.drawer.draw(qpainter)
        qpainter.end()
    
    def updateCellValue(self, mouseX: int, mouseY, value: int=None):
        if not self.board or (not self.editMode.isChecked() and value is None):
            return
        
        board = self.board
        x, y = board.get_cell(mouseX, mouseY)
        if not board.is_pos_valid(x, y) or (value and board.get_cell_value(x, y) < 0):
            return
        
        if value != None:
            board.cells[y][x] = value
            return x, y
        
        if self.mouseButton == Qt.LeftButton:
            board.cells[y][x] = 0
        elif self.mouseButton == Qt.RightButton:
            board.cells[y][x] = -1
        
    def mousePressEvent(self, event: QEvent):
        x, y, button = event.x(), event.y(), event.button()
        self.mouseButton = button
        
        if self.newPoints != None:  # если режим выбора точек
            value = -2  # не задана начальная
            if self.newPoints:  # не задана конечная
                value = -3
            
            pos = self.updateCellValue(x, y, value)
            if pos:
                self.newPoints.append(pos)
            
            if len(self.newPoints) == 2:
                self.points = self.newPoints
                self.newPoints = None
                
                self.board.set_cell_value(*self.points[0], 0)
                self.board.set_cell_value(*self.points[1], 0)
                self.resetBoard()
                self.solve()
                
        else:
            self.updateCellValue(x, y)

        self.repaint()
        
    def mouseReleaseEvent(self, event: QEvent):
        self.mouseButton = -1
    
    def mouseMoveEvent(self, event: QEvent):
        self.updateCellValue(event.x(), event.y())
        self.repaint()
                
    def keyPressEvent(self, event: QEvent):
        if self.drawer:
            self.drawer.modifiers = event.modifiers()
            self.repaint()
    
    def keyReleaseEvent(self, event: QEvent):
        if self.drawer:
            self.drawer.modifiers = event.modifiers()
            self.repaint()
    
    def resizeEvent(self, event: QEvent):
        return
    
        size = event.size()
        width, height = size.width(), size.height()
        
        px_w = width - self.board.px_off_left - self.board.px_off_right
        px_h = height - self.board.px_off_top - self.board.px_off_bottom
        
        self.board.cell = min(px_w, px_h) / 16

            
def main():
    sys.excepthook = lambda c, e, t: sys.__excepthook__(c, e, t)
    app = QApplication(sys.argv)
    
    main_w = MWindow()
    main_w.show()
    
    sys.exit(app.exec())

 
if __name__ == "__main__": 
    main()
